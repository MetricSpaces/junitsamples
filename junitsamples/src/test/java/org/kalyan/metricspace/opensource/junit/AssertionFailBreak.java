package org.kalyan.metricspace.opensource.junit;

import org.junit.Test;
import org.junit.rules.ErrorCollector;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;

public class AssertionFailBreak {
	
	/*@Test
	public void test(){
		System.out.println("Hello");
		assertEquals(10, 10);
		System.out.println("World");
		System.out.println("Testing"); 
		assertEquals(10, 15);
		System.out.println("This statement will never be printed"); 
	}*/
	
	/*@Test
	public void testWithTryCatch(){
		System.out.println("Hello");
		assertEquals(10, 10);
		System.out.println("World");
		System.out.println("Testing");
		try{
			assertEquals(10, 15);
		} catch(Throwable e){
			System.out.println("This test failed");
		}
		
		System.out.println("This statement will be printed"); 
	}*/
	 @Rule
	 public ErrorCollector collector= new ErrorCollector();
	
	@Test
	public void testWithEC(){
		
		System.out.println("Hello");
		assertEquals(10, 10);
		System.out.println("World");
		System.out.println("Testing");
		try{
			assertEquals(10, 15);
		} catch(Throwable e){
			collector.addError(e);
		}
		
		System.out.println("This statement will be printed"); 
	}
	
	
	

}
