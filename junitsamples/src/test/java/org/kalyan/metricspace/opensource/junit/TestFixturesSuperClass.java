/**
 * 
 */
package org.kalyan.metricspace.opensource.junit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Kalyan Sarkar
 *
 */
public class TestFixturesSuperClass {
	
	@BeforeClass public static void beforeClass(){
		System.out.println("Superclass before class");
	}
	
	
	@Before public void before(){
		System.out.println("Superclass before");
	}
	
	@Test public void test1(){
		System.out.println("Superclass test 1");
	}
	
	@Test public void test2(){
		System.out.println("Superclass test 2");
	}
	
	@After public void after(){
		System.out.println("Superclass after");
	}
	
	@AfterClass public static void afterClass(){
		System.out.println("Superclass after class");
	}

}
