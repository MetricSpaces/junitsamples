package org.kalyan.metricspace.opensource.junit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Kalyan Sarkar
 *
 */
public class TestFixturesSubClass extends TestFixturesSuperClass {
	
	/*@BeforeClass public static void beforeClass(){
		System.out.println("Subclass before class");
	}
	
	
	@Before public void before(){
		System.out.println("Subclass before");
	}*/
	
	@Test public void test1(){
		System.out.println("Subclass test 1");
	}
	
	@Test public void test2(){
		System.out.println("Subclass test 2");
	}
	
	/*@After public void after(){
		System.out.println("Subclass after");
	}
	
	@AfterClass public static void afterClass(){
		System.out.println("Subclass after class");
	}*/

}
