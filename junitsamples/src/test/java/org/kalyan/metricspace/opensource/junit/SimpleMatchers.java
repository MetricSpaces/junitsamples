package org.kalyan.metricspace.opensource.junit;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * 
 * @author Kalyan
 *
 */

@SuppressWarnings("unchecked")
public class SimpleMatchers {

	@Test
	public void testAny() {
		final String str = "Hello World";
		assertThat(str, any(String.class));
		assertThat(str, is(any(String.class)));
	}

	@Test
	public void testAnything() {
		String str = "Hello World";
		assertThat(str, anything());
		assertThat(str, is(anything()));
		str = null;
		assertThat(str, anything());
		assertThat(str, is(anything()));
	}

	@Test
	public void testArrayContaining() {
		String[] arr = { "This", "is", "Hello", "World" };
		// Does the array contain all given items in the order
		// in which they are input to the matcher?
		assertThat(arr, is(arrayContaining("This", "is", "Hello", "World")));
		// Does the array contain items which match the input list of matchers,
		// in order?
		assertThat(
				arr,
				is(arrayContaining(equalTo("This"), equalTo("is"),
						equalTo("Hello"), equalTo("World"))));
		// Does the array contain items which match the input vararg matchers,
		// in order?
		assertThat(
				arr,
				is(arrayContaining(startsWith("This"), equalTo("is"),
						endsWith("Hello"), endsWith("World"))));

	} 
	
	@Test
	public void testArrayContainingInAnyOrder() {
		String[] arr = { "This", "is", "Hello", "World" };
		// Does the array contain all given items in any order
		assertThat(arr, is(arrayContainingInAnyOrder("is", "Hello", "World", "This")));
		//This will fail
		//assertThat(arr, is(arrayContainingInAnyOrder("is", "Hello", "World", "This","is")));
		// Does the array contain items which match the input list of matchers in any order?
		assertThat(
				arr,
				is(arrayContainingInAnyOrder(equalTo("This"), equalTo("is"),
						equalTo("Hello"), equalTo("World"))));
		// Does the array contain items which match the input vararg matchers in any order?
		assertThat(
				arr,
				is(arrayContainingInAnyOrder(startsWith("This"), equalTo("is"),
						endsWith("Hello"), endsWith("World"))));

	}
	
	@Test
	public void testArrayOfSize() {
		String[] arr = { "This", "is", "Hello", "World" };
		//Does the input array have exactly the specified length?
		assertThat(arr, is(arrayWithSize(4)));
		//Does the input array have a length which matches the specified matcher?
		assertThat(arr, is(arrayWithSize(greaterThan(2))));
	}

}
