package org.kalyan.metricspace.opensource.junit;


import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

/**
 * 
 * @author Kalyan
 *
 */
public class SimpleHamcrestTest {
	
	@Test
	public void test(){
		// Simple JUnit assert
		assertEquals("a","a");
		
		// Hamcrest matchers
		assertThat("a", is("a"));
		assertThat("a", equalTo("a"));
		assertThat("a", is(equalTo("a")));
	}

}
