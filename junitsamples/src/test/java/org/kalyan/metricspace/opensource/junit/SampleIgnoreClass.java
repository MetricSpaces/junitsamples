package org.kalyan.metricspace.opensource.junit;


import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;
/**
 * 
 * @author Kalyan.Sarkar
 *
 */
@Ignore("ignore the test case")
public class SampleIgnoreClass {
	
	
	@Test
	public void test(){
		// If this test runs it will fail forcibly.
		fail();
	}

}
