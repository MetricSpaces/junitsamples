package org.kalyan.metricspace.opensource.junit;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * 
 * @author Kalyan Sarkar
 *
 */
@RunWith(Parameterized.class)
public class ParameterizedAdditionTester {
	
	private int a;
	private int b;
	private int expected;
	private Addition addition;
	
	public ParameterizedAdditionTester(int a,int b,int expected){
		this.a = a;
		this.b = b;
		this.expected = expected;
	}
	
	@Before
	public void setup(){
		addition = new Addition();
	}
	
	@Parameters
	public static Collection input(){
		return Arrays.asList(new Object[][]{ 
			{5,5,10},{6,4,10},{4,6,10},{7,3,10},
			{3,7,10}, {8,2,10}, {2,8,10}});
	}
	
	@Test
	public void addTest(){
		assertEquals(expected, addition.add(a, b));
	}
	
	@After
	public void teardown(){
		addition = null;
	}

}
