package org.kalyan.metricspace.opensource.junit;

import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * 
 * @author Kalyan Sarkar
 *
 */
public class NaiveAdditionTester {
	
	Addition addition;
	
	@Before
	public void setup(){
		addition = new Addition();
	}
	
	@Test
	public void addTest(){
		assertEquals(10, addition.add(5, 5));
		assertEquals(10, addition.add(6, 4));
		assertEquals(10, addition.add(4, 6));
		assertEquals(10, addition.add(7, 3));
		assertEquals(10, addition.add(3, 7));
		assertEquals(10, addition.add(8, 2));
		assertEquals(10, addition.add(2, 8));
	}
	
	@After
	public void teardown(){
		addition = null;
	}

}
