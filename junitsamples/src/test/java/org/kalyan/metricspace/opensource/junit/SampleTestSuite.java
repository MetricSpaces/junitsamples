package org.kalyan.metricspace.opensource.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ExceptionAndFailedTests.class,TestInstallation.class})
public class SampleTestSuite {
	

}
