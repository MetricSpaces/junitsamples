package org.kalyan.metricspace.opensource.junit;

import org.junit.Test;

/**
 * 
 * @author Kalyan Sarkar
 *
 */
public class TimeoutTesting {
	
	@Test(timeout=100)
	public void timeoutFail() throws InterruptedException{
		Thread.sleep(110);
	}
	
	@Test(timeout=100)
	public void timeoutPass() throws InterruptedException{
		Thread.sleep(90);
	}

}
