package org.kalyan.metricspace.opensource.junit;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * 
 * @author Kalyan Sarkar
 *
 */
public class TestInstallation {
	
	@Test
	public void test(){
		final String str = "Testing the junit installation";
		assertEquals(str, "Testing the junit installation");
		
	}

}
