package org.kalyan.metricspace.opensource.junit;


import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;
/**
 * 
 * @author Kalyan.Sarkar
 *
 */
public class SampleIgnoreMethod {
	
	@Ignore("ignore the test case")
	@Test
	public void test(){
		// If this test runs it will fail forcibly.
		fail();
	}

}
