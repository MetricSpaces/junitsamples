package org.kalyan.metricspace.opensource.junit;

import org.junit.Test;

/**
 * 
 * @author 212411412
 *
 */
public class ExceptionTest {
	
	@Test(expected=RuntimeException.class)
	public void failedTest(){
		
	}
	
	@Test(expected=RuntimeException.class)
	public void successfulTest(){
		throw new RuntimeException("useless exception");
	}
	
	@Test(expected=Throwable.class)
	public void superclassExceptionTest(){
		throw new RuntimeException("expects Throwable");
	}

}
