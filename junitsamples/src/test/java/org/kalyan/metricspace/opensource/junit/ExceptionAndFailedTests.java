package org.kalyan.metricspace.opensource.junit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 
 * @author 212411412
 *
 */
public class ExceptionAndFailedTests {
	
	@Test
	public void test1() throws Exception{
		final String str = "This test will throw exception";
		assertEquals(str, "This test will throw exception");
		throw new Exception();
		
	}
	
	@Test
	public void test2(){
		final String str = "This test will fail";
		assertEquals(str, "This test will fail terribly");
	}
	
	@Test
	public void test3(){
		final String str = "But the final test will run";
		assertEquals(str, "But the final test will run");
	}

}
